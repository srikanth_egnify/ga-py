class RankAnalysis:
    def __init__(self, studentId, testId, hierarchy, masterResult, subjectWiseRanks={}):
        self.studentId = studentId
        self.testId = testId
        self.hierarchy = hierarchy
        self.rankAnalysis = {}
        self.populateData(masterResult, subjectWiseRanks)

    def populateData(self, masterResult, subjectWiseRanks):
        overallTotalMarks = 0
        overallTotalObtainedMarks = 0
        for subject, data in masterResult["topicAnalysis"].items():
            totalMarks = data["totalMarks"]
            totalObtainedMarks = data["totalObtainedMarks"]
            percentage = (totalObtainedMarks / totalMarks) * 100.0
            # init the rank analysis for subject
            rank = subjectWiseRanks.get(subject, 0)
            self.rankAnalysis[subject] = {
                "rank": rank,
                "totalMarks": totalMarks,
                "totalObtainedMarks": totalObtainedMarks,
                "percentage": percentage,
            }
            # sum up individual subject marks with overall marks
            overallTotalMarks += totalMarks
            overallTotalObtainedMarks += totalObtainedMarks
        # add overall data
        overallRank = subjectWiseRanks.get("overall", 0)
        overallPercentage = (overallTotalObtainedMarks / overallTotalMarks) * 100.0
        self.rankAnalysis["overall"] = {
            "rank": overallRank,
            "totalMarks": overallTotalMarks,
            "totalObtainedMarks": overallTotalObtainedMarks,
            "percentage": overallPercentage,
        }

    def subjectData(self, subject="overall"):
        data = self.rankAnalysis[subject]
        return data["rank"], data["totalMarks"], data["totalObtainedMarks"]

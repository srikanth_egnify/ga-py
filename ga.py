import functools
import multiprocessing
import pprint
import time
from collections import defaultdict, OrderedDict
from dataclasses import InitVar, asdict, dataclass, field
from typing import DefaultDict

from pymongo import MongoClient, InsertOne, errors

import priority_queue
from master_result import MasterResult
from rank_analysis import RankAnalysis
from hierarchy_analysis import calculate_analysis
from db.HydraMongoDb import HydraMongoDb

hydra_db = HydraMongoDb("mongodb://localhost:27017")

client = MongoClient("mongodb://localhost:27017")

settings_db = client["ui-china-settings-qa"]
test_db = client["ui-china-test-management-qa"]


def get_snapshot_docs(test_id):
    snap_query = {"testId": test_id}
    snap_coll = test_db["testStudentSnapshot"]
    return list(snap_coll.find(snap_query))


def get_hierarchy_adj_list(snap_docs):
    adj_list = defaultdict(list)
    hierarchies_coll = settings_db["institutehierarchies"]
    hierarchies = list(hierarchies_coll.find())
    # prepare adj_list for hierarchies
    for hierarchy in hierarchies:
        parent_code = hierarchy.get("parentCode")
        child_code = hierarchy.get("childCode")
        if parent_code and child_code:
            adj_list[parent_code].append(child_code)
    # add student to adj_list of thier immediate hierarchy
    for doc in snap_docs:
        student_hierarchy = doc["accessTag"]["hierarchy"]
        student_id = doc["studentId"]
        adj_list[student_hierarchy].append(student_id)
    return adj_list


def get_question_mapping(test_id):
    q_map_query = {"testId": test_id}
    q_map_coll = test_db["questionmappings"]
    q_mappings = q_map_coll.find(q_map_query)
    return {x["questionNumber"]: x for x in q_mappings}


def calculate_master_results(snap_doc, question_mapping):
    mr = MasterResult(
        snap_doc["studentId"],
        snap_doc["testId"],
        snap_doc["responseData"]["questionResponse"],
        question_mapping,
    )
    return mr.__dict__


def generate_rank_analysis(
    hierarchy, adj_list, master_results, rank_analysis, tie_break_list
):
    # recurse for rank analysis in `hierarchy` if child nodes present
    child_nodes = adj_list[hierarchy]
    if child_nodes:
        child_nodes_data = OrderedDict()
        for child_node in child_nodes:
            child_data = generate_rank_analysis(
                child_node, adj_list, master_results, rank_analysis, tie_break_list
            )
            child_nodes_data.update(child_data)
        ordered_child_nodes_data = child_nodes_data
        # calculate subject wise hierarchy ranks
        subject_wise_ranks = defaultdict(dict)
        for subject in tie_break_list:
            for student in ordered_child_nodes_data.values():
                _, totalMarks, obtainedMarks = student.subjectData(subject)
                studentId = student.studentId
                priority = totalMarks - obtainedMarks
                priority_queue.add_student(studentId, priority)
            ordered_child_nodes_data = OrderedDict()
            rank = 0
            while priority_queue.pq:
                rank += 1
                student = priority_queue.pop_student()
                subject_wise_ranks[student][subject] = rank
                ordered_child_nodes_data[student] = child_nodes_data[student]
        return_data = OrderedDict()
        for student in ordered_child_nodes_data.values():
            studentId = student.studentId
            testId = student.testId
            student_subject_ranks = subject_wise_ranks[studentId]
            sra = RankAnalysis(
                studentId,
                testId,
                hierarchy,
                master_results[studentId],
                student_subject_ranks,
            )
            return_data[studentId] = sra
        rank_analysis[hierarchy] = [item.__dict__ for item in return_data.values()]
        return return_data
    else:
        return_data = OrderedDict()
        # reached lowest level possible i.e student
        if hierarchy in master_results:
            student_data = master_results[hierarchy]
            student_rank = RankAnalysis(
                student_data["studentId"],
                student_data["testId"],
                hierarchy,
                student_data,
            )
            return_data[hierarchy] = student_rank
        return return_data


def generate_hierarchy_analysis(
    hierarchy, adj_list, master_results, mark_analysis, hierarchical_analysis
):
    child_nodes = adj_list[hierarchy]
    childMarkAnalysis, childTopicAnalysis = {}, {}
    if child_nodes:
        child_nodes_data = defaultdict(list)
        ma_append = child_nodes_data["marks"].append
        ta_append = child_nodes_data["topic"].append
        for child_node in child_nodes:
            ma, ta = generate_hierarchy_analysis(
                child_node,
                adj_list,
                master_results,
                mark_analysis,
                hierarchical_analysis,
            )
            if ma and ta:
                ma_append(ma)
                ta_append(ta)

        childMarkAnalysis, childTopicAnalysis = calculate_analysis(child_nodes_data)
        mark_analysis[hierarchy] = childMarkAnalysis
        hierarchical_analysis[hierarchy] = childTopicAnalysis
        return childMarkAnalysis, childTopicAnalysis
    else:
        # reached the lowest level possible could be student or institute hierarchy
        if hierarchy in master_results:
            student_data = master_results[hierarchy]
            topicAnalysis = student_data["topicAnalysis"]
            for subject, subjectData in topicAnalysis.items():
                childMarkAnalysis[subject] = {str(subjectData["totalObtainedMarks"]): 1}
            childTopicAnalysis.update(topicAnalysis)
        return childMarkAnalysis, childTopicAnalysis


def launch(data):
    test_id = data["testId"]
    step_one = time.process_time()
    # fetch the snapshot documents for test
    snap_docs = get_snapshot_docs(test_id)
    # prepare adjacency list for hierarchies, here we are passing snap_docs
    # to append students to adjacency list of their immediate hierarchy
    adj_list = get_hierarchy_adj_list(snap_docs)
    # fetch the question mappings for test
    q_mappings = get_question_mapping(test_id)
    elapsed = time.process_time() - step_one
    print("Time taken to get pre required data from database", elapsed)
    step_two = time.process_time()
    mr_pool = multiprocessing.Pool(multiprocessing.cpu_count())
    master_results = mr_pool.map(
        functools.partial(calculate_master_results, question_mapping=q_mappings),
        snap_docs,
        2500,
    )
    master_results_map = {item["studentId"]: item for item in master_results}
    print("Time taken to calculate master results", time.process_time() - step_two)
    step_three = time.process_time()
    rank_analysis = {}
    tie_break_list = ["Physics", "Chemistry", "Maths", "overall"]
    generate_rank_analysis(
        "Egni_u001_l11", adj_list, master_results_map, rank_analysis, tie_break_list
    )
    print("Time taken to generate rank analysis", time.process_time() - step_three)
    step_four = time.process_time()
    mark_analysis = {}
    hierarchical_analysis = {}
    generate_hierarchy_analysis(
        "Egni_u001_l11", adj_list, master_results_map, mark_analysis, hierarchical_analysis
    )
    print("Time taken to generate hierarchy analysis", time.process_time() - step_four)

    step_five = time.time()
    mr_bulk = [InsertOne(item) for item in master_results]
    ra_records = []
    extend = ra_records.extend
    for lst in rank_analysis.values():
        extend(lst)
    ra_bulk = [InsertOne(item) for item in ra_records]
    ma_bulk = [InsertOne(item) for item in mark_analysis.values()]
    ha_bulk = [InsertOne(item) for item in hierarchical_analysis.values()]
    try:
        test_db["masterresults"].bulk_write(mr_bulk, ordered=False)
        test_db["rankAnalysis"].bulk_write(ra_bulk, ordered=False)
        test_db["markAnalysis"].bulk_write(ma_bulk, ordered=False)
        test_db["hierarchicalAnalysis"].bulk_write(ha_bulk, ordered=False)
    except errors.BulkWriteError as bwe:
        print(bwe.details)
        #you can also take this component and do more analysis
        #werrors = bwe.details['writeErrors']
    
    # hydra_db.upsert_many_in_process("ui-china-test-management-qa", "masterresults", master_results)
    # hydra_db.insert_many_in_process("ui-china-test-management-qa", "rankAnalysis", rank_analysis.values())
    # hydra_db.insert_many_in_process("ui-china-test-management-qa", "markAnalysis", mark_analysis.values())
    # hydra_db.insert_many_in_process("ui-china-test-management-qa", "hierarchicalAnalysis", hierarchical_analysis.values())
    hydra_db.wait_for_termination()
    print("Time taken to write to db", time.time() - step_five)


launch(data={"testId": "000011"})

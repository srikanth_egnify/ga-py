from collections import defaultdict


def emptyTopicAnalysis():
    return {
        "totalMarks": 0,
        "totalObtainedMarks": 0,
        "totalQuestion": 0,
        "numberOfQuestions": 0,
        "aggregatedMarks": 0,
        "minMarks": int(1e9),
        "maxMarks": -int(1e9),
        "percentage": 0.0,
        "C": 0,
        "W": 0,
        "U": 0,
        "P": 0,
        "ADD": 0,
        "UW": 0,
        "next": {},
    }

def fill_topic_data(first, second):
    first["totalMarks"] += second["totalMarks"]
    first["totalObtainedMarks"] += second["totalObtainedMarks"]
    first["totalQuestion"] += second["totalQuestion"]
    first["minMarks"] = min(first["minMarks"], second["minMarks"])
    first["maxMarks"] = max(first["maxMarks"], second["maxMarks"])
    first["C"] += second["C"]
    first["W"] += second["W"]
    first["U"] += second["U"]
    first["P"] += second["P"]
    first["ADD"] += second["ADD"]

def calculate_analysis(child_nodes_data):
    ma = defaultdict(lambda: defaultdict(int))
    ta = {}
    fill_data = fill_topic_data
    for mark_data in child_nodes_data["marks"]:
        for subject, subject_data in mark_data.items():
            for marks, count in subject_data.items():
                ma[subject][marks] += count
    for topic_data in child_nodes_data["topic"]:
        for subject, subject_data in topic_data.items():
            for topic, topic_data in subject_data["next"].items():
                for sub_topic, sub_topic_data in topic_data["next"].items():
                    prev_subject_data = ta.get(subject, emptyTopicAnalysis())
                    prev_topic_data = prev_subject_data["next"].get(
                        topic, emptyTopicAnalysis()
                    )
                    prev_subtopic_data = prev_topic_data["next"].get(
                        sub_topic, emptyTopicAnalysis()
                    )
                    fill_data(prev_subject_data, subject_data)
                    fill_data(prev_topic_data, topic_data)
                    fill_data(prev_subtopic_data, sub_topic_data)
                    ta[subject] = prev_subject_data
                    ta[subject]["next"][topic] = prev_topic_data
                    ta[subject]["next"][topic]["next"][sub_topic] = prev_subtopic_data
    return ma, ta

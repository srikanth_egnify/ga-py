import itertools
from heapq import heappush, heappop

pq = []  # list of entries arranged in a heap
entry_finder = {}  # mapping of students to entries
REMOVED = "<removed-student>"  # placeholder for a removed student
counter = itertools.count()  # unique sequence count


def add_student(student, priority=0):
    "Add a new student or update the priority of an existing student"
    if student in entry_finder:
        remove_student(student)
    count = next(counter)
    entry = [priority, count, student]
    entry_finder[student] = entry
    heappush(pq, entry)


def remove_student(student):
    "Mark an existing student as REMOVED.  Raise KeyError if not found."
    entry = entry_finder.pop(student)
    entry[-1] = REMOVED


def pop_student():
    "Remove and return the lowest priority student. Raise KeyError if empty."
    while pq:
        priority, count, student = heappop(pq)
        if student is not REMOVED:
            del entry_finder[student]
            return student
    raise KeyError("pop from an empty priority queue")


if __name__ == "__main__":
    import random
    import time

    max_students, students = 30000, []
    start = time.process_time()
    for i in range(max_students):
        student = {"priority": random.randint(1, max_students), "student_id": i}
        students.append(student)
        add_student(student["student_id"], student["priority"])
    while pq:
        student_id = pop_student()
    end = time.process_time() - start
    print("%.3f" % end)

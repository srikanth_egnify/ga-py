from collections import defaultdict


class MasterResult:
    def __init__(self, studentId, testId, rawResponse, questionMapping):
        self._id = "_".join([studentId, testId])
        self.studentId = studentId
        self.testId = testId
        self.responseData = {}
        self.topicAnalysis = {}
        self.cwuAnalysis = {}
        self.populateQuestionResponse(rawResponse, questionMapping)
        self.populateTopicAnalysis(questionMapping)
        self.populateCWUAnalysis(questionMapping)

    def populateQuestionResponse(self, rawResponse, questionMapping):
        qr = {}
        for question, response in rawResponse.items():
            qData = questionMapping[question]
            qr[question] = {}
            qr[question]["questionNumber"] = question
            qr[question]["subject"] = qData["subject"]
            qr[question]["topic"] = qData["topic"]
            qr[question]["subTopic"] = qData["subTopic"]
            qr[question]["response"] = response
        self.responseData = qr

    def populateTopicAnalysis(self, questionMapping):
        for q, data in self.responseData.items():
            response = data["response"]
            obtainedMarks = questionMapping[q][response]
            marksAllocated = questionMapping[q]["C"]
            subjet, topic, subTopic = data["subject"], data["topic"], data["subTopic"]
            self.checkEmpty(subjet, topic, subTopic)
            subjectAnalysis = self.topicAnalysis[subjet]
            topicAnalysis = subjectAnalysis["next"][topic]
            subTopicAnalysis = topicAnalysis["next"][subTopic]

            subjectAnalysis["totalMarks"] += marksAllocated
            subjectAnalysis["totalObtainedMarks"] += obtainedMarks
            subjectAnalysis["totalQuestion"] += 1
            subjectAnalysis["numberOfQuestions"] += 1
            subjectAnalysis["minMarks"] = min(
                subjectAnalysis["minMarks"], obtainedMarks
            )
            subjectAnalysis["maxMarks"] = max(
                subjectAnalysis["maxMarks"], obtainedMarks
            )
            subjectAnalysis[response] += 1

            topicAnalysis["totalMarks"] += marksAllocated
            topicAnalysis["totalObtainedMarks"] += obtainedMarks
            topicAnalysis["totalQuestion"] += 1
            topicAnalysis["numberOfQuestions"] += 1
            topicAnalysis["minMarks"] = min(topicAnalysis["minMarks"], obtainedMarks)
            topicAnalysis["maxMarks"] = max(topicAnalysis["maxMarks"], obtainedMarks)
            topicAnalysis[response] += 1

            subTopicAnalysis["totalMarks"] += marksAllocated
            subTopicAnalysis["totalObtainedMarks"] += obtainedMarks
            subTopicAnalysis["totalQuestion"] += 1
            subTopicAnalysis["numberOfQuestions"] += 1
            subTopicAnalysis["minMarks"] = min(
                subTopicAnalysis["minMarks"], obtainedMarks
            )
            subTopicAnalysis["maxMarks"] = max(
                subTopicAnalysis["maxMarks"], obtainedMarks
            )
            subTopicAnalysis[response] += 1

    def populateCWUAnalysis(self, questionMapping):
        # iterate over response data
        for q, data in self.responseData.items():
            response = data["response"]
            obtainedMarks = questionMapping[q][response]
            subject = data["subject"]
            # fill empty cwu data if subject doesn't already exist
            if subject not in self.cwuAnalysis:
                self.cwuAnalysis[subject] = self.emptyCWU()
            # increment corresponding response count
            self.cwuAnalysis[subject][response] += 1
            # increment corresponding response marks count
            self.cwuAnalysis[subject]["marks"][response] += obtainedMarks

    def checkEmpty(self, subject, topic, subTopic):
        if subject not in self.topicAnalysis:
            self.topicAnalysis[subject] = self.emptyTopicAnalysis()
        subjectAnalysis = self.topicAnalysis[subject]
        if topic not in subjectAnalysis["next"]:
            self.topicAnalysis[subject]["next"][topic] = self.emptyTopicAnalysis()
        topicAnalysis = self.topicAnalysis[subject]["next"][topic]
        if subTopic not in topicAnalysis["next"]:
            self.topicAnalysis[subject]["next"][topic]["next"][
                subTopic
            ] = self.emptyTopicAnalysis()

    def emptyCWU(self):
        return {
            "C": 0,
            "W": 0,
            "U": 0,
            "ADD": 0,
            "P": 0,
            "marks": {"C": 0, "W": 0, "U": 0, "ADD": 0, "P": 0,},
        }

    def emptyTopicAnalysis(self):
        return {
            "totalMarks": 0,
            "totalObtainedMarks": 0,
            "totalQuestion": 0,
            "numberOfQuestions": 0,
            "aggregatedMarks": 0,
            "minMarks": int(1e9),
            "maxMarks": -int(1e9),
            "percentage": 0.0,
            "C": 0,
            "W": 0,
            "U": 0,
            "P": 0,
            "ADD": 0,
            "UW": 0,
            "next": {},
        }
